# Set Updater operator

This operator checks a kubernetes `customResouce` to see what size it should use for the number of replicas in its stateful set. This idea can be further abstracted to gather data from outside sources to to have more dynamic verification of state. Additionally you can with one single `POST` to the cluster update any value within the StatefulSet's configuration.

## How to run

This project was built using the `operator-sdk` tool found [here](https://github.com/operator-framework/operator-sdk) and the image is hosted on Quay [here](https://quay.io/repository/frenchtoasters/podkill-operator). These are the steps you need to run this:

* First you need to clone this repo:

```
git clone https://gitlab.com/frenchtoasters/set-updater.git
```

* Then you need to build your local image:

```
> docker login quay.io

> operator-sdk build quay.io/frenchtoasters/podkill-operator

> docker push quay.io/frenchtoasters/podkill-operator
```

* Then you will probably want these commands unless you are using my image:

```
> sed -i 's|quay.io/frenchtoasters/podkill-operator|quay.io/XXXX/XXXX|g' deploy/operator.yaml
```

* Next you will need to deploy the following into your cluster:

```
> kubectl create -f deploy/service_account.yaml
> kubectl create -f deploy/role.yaml
> kubectl create -f deploy/role_binding.yaml
> kubectl create -f deploy/crds/app_v1alpha1_podkillerservice_crd.yaml
> kubectl create -f deploy/operator.yaml
> kubectl create -f deploy/crds/app_v1alpha1_podkillerservice_cr.yaml
```

* Then run a 

```
> kubectl get pod -l app=example-podkiller
```
