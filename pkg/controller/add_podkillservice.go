package controller

import (
	"github.com/pod-killer/podkill-operator/pkg/controller/podkillservice"
)

func init() {
	// AddToManagerFuncs is a list of functions to create controllers and add them to a manager.
	AddToManagerFuncs = append(AddToManagerFuncs, podkillservice.Add)
}
